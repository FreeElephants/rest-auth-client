#!/usr/bin/env bash
docker run --rm --interactive --tty \
    --volume $PWD:/app \
    --workdir /app \
    php:7.0.17 vendor/bin/codecept "$@"