#!/usr/bin/env bash

function printInfo {
    COLOR='\033[1;33m'
    NO_COLOR='\033[0m'
    printf "${COLOR}=== $1${NO_COLOR}\n"
}