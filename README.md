# PHP Rest Auth Client

## Installation

Add dependency and repo to composer.json: 
```
    "require": {
        "free-elephants/rest-auth-client": "^0.5"
    }
    "repositories": {
        {
            "type": "vcs",
            "url": "git@bitbucket.org:FreeElephants/rest-auth-client.git"
        }    
    }
```

## Usage
Auth Client provide methods for interaction with [Rest Auth Service API](https://bitbucket.org/FreeElephants/rest-auth).

### Game Users Api Client
Add client instance as dependency to IoC-container of your project. 
In Constructor RestClient accept Http client instances.   
```
$authClient = new \FreeElephants\RestAuthClient\RestClient(new \FreeElephants\RestAuthClient\HttpAdapter\GuzzleAdapter('https://your-rest-auth-address:port/api/v1/'));
$di->setService(\FreeElephants\RestAuthClient\AuthClientInterface::class, $authClient);
```
For constructing your owned User model, you can pass custom UserFactoryInterface implementation as second argument. User model must implement \FreeElephants\RestAuthClient\Model\UserInterface.    

Check User Authorization:
```
$authKey = $request->getHeaderLine('Authorization');
if($authClient->isValidAuthKey($authKey)) {
    // work with logined user
} else {
    $response = $responce->withStatus(401);
}
```

Get User object by Auth key: 
```
$authKey = $request->getHeaderLine('Authorization');
if($authClient->isValidAuthKey($authKey)) {
    $user = $authClient->getUserByAuthKey();   
}
```

Get User by its ID:
```
$user = $authClient->getUserById($id);
```

Get all users
```
$users = $authClient->getUsers();
```

## Errors Handling and Exceptions:
Methods ::isValidAuthKey(), ::getUserByAuthKey() 
- Throws `\FreeElephants\RestAuthClient\Exception\RuntimeException` in case when Auth Api response has error status. Actual status available in exception code.  
Method ::getUserByAuthKey()
- Throw `\FreeElephants\RestAuthClient\Exception\Domain` in case when given auth key not found.  

## For Contributors
We use docker in development and testing. You does not need something except git and docker on your local machine. 

After clone you can install vendors with run next script:
```bash
./bash/composer.sh install --ignore-platform-reqs
```
For execute test suite use codeception in conteiner with this command:
 ```bash
./bash/codecept.sh run
```

Wrappers forward all arguments to original script. 

See bitbucket-pipelines.yml for [CI](https://bitbucket.org/FreeElephants/rest-auth-client/addon/pipelines/home#!/) configuration. All 