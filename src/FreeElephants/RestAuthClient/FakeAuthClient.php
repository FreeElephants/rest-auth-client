<?php

namespace FreeElephants\RestAuthClient;

use FreeElephants\RestAuthClient\Exception\DomainException;
use FreeElephants\RestAuthClient\Exception\RuntimeException;
use FreeElephants\RestAuthClient\Model\UserInterface;

/**
 * @internal it's fake for testing.
 * @author samizdam <samizdam@inbox.ru>
 */
class FakeAuthClient implements AuthClientInterface
{

    /**
     * @var array|UserInterface[]
     */
    private $authKeyUserMap;

    public function __construct(array $authKeyUserMap)
    {
        $this->authKeyUserMap = $authKeyUserMap;
    }

    public function isAuthKeyValid(string $authKey): bool
    {
        return isset($this->authKeyUserMap[$authKey]);
    }

    public function getUserByAuthKey(string $authKey): UserInterface
    {
        if ($this->isAuthKeyValid($authKey)) {
            return $this->authKeyUserMap[$authKey];
        } else {
            throw new RuntimeException();
        }
    }

    /**
     * @return array|UserInterface[]
     */
    public function getUsers(): array
    {
        return array_values($this->authKeyUserMap);
    }

    public function getUserById($id): UserInterface
    {
        foreach ($this->authKeyUserMap as $item) {
            if($item->getId() === $id) {
                return $item;
            }
        }

        throw new DomainException('User with given id not found. ');
    }
}