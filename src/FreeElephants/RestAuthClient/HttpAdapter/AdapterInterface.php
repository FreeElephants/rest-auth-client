<?php

namespace FreeElephants\RestAuthClient\HttpAdapter;

use Psr\Http\Message\ResponseInterface;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
interface AdapterInterface
{

    public function get($path, array $queryParams = [], array $headers = []): ResponseInterface;

    public function head($path, array $queryParams = [], array $headers = []): ResponseInterface;
}