<?php

namespace FreeElephants\RestAuthClient;

use FreeElephants\RestAuthClient\Exception\DomainException;
use FreeElephants\RestAuthClient\Exception\RuntimeException;
use FreeElephants\RestAuthClient\HttpAdapter\AdapterInterface;
use FreeElephants\RestAuthClient\HttpAdapter\GuzzleAdapter;
use FreeElephants\RestAuthClient\Model\DefaultJsonMappingUserFactory;
use FreeElephants\RestAuthClient\Model\User;
use FreeElephants\RestAuthClient\Model\UserFactoryInterface;
use FreeElephants\RestAuthClient\Model\UserInterface;
use GuzzleHttp\Client;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class RestClient implements AuthClientInterface
{

    const DEFAULT_BASE_URI = 'http://127.0.0.1:8080/api/v1/';
    /**
     * @var AdapterInterface
     */
    private $httpClient;
    /**
     * @var UserFactoryInterface
     */
    private $userFactory;

    public function __construct(AdapterInterface $httpClient = null, UserFactoryInterface $userFactory = null)
    {
        $this->httpClient = $httpClient ?: new GuzzleAdapter(new Client(['base_uri' => self::DEFAULT_BASE_URI]));
        $this->userFactory = $userFactory ?: new DefaultJsonMappingUserFactory();
    }

    public function isAuthKeyValid(string $authKey): bool
    {
        $response = $this->httpClient->head($authKey);
        $status = $response->getStatusCode();
        if ($status === 200 || $status === 204) {
            return true;
        } elseif ($status === 404) {
            return false;
        }
        throw new RuntimeException('Api responded with error. ', $status);
    }

    public function getUserByAuthKey(string $authKey): UserInterface
    {
        $response = $this->httpClient->get($authKey);
        $status = $response->getStatusCode();
        if ($status === 200) {
            $body = $response->getBody();
            $body->rewind();
            $userData = json_decode($body->getContents(), JSON_OBJECT_AS_ARRAY);
            return $this->userFactory->createUser($userData);
        } elseif ($status === 404) {
            throw new DomainException('Given auth key not exists');
        }
        throw new RuntimeException('Api responded with error. ', $status);
    }

    public function getUsers(): array
    {
        $response = $this->httpClient->get('users');
        $status = $response->getStatusCode();
        if ($status === 200) {
            $body = $response->getBody();
            $body->rewind();
            $usersData = json_decode($body->getContents(), JSON_OBJECT_AS_ARRAY);
            return array_map(function ($userData) {
                return new User($userData['id'], $userData['login']);
            }, $usersData['items']);
        } else {
            throw new RuntimeException('Api responsed with error.', $status);
        }
    }

    public function setHttpClient(AdapterInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getUserById($id): UserInterface
    {
        $response = $this->httpClient->get('users/' . $id);
        $status = $response->getStatusCode();
        if ($status === 200) {
            $body = $response->getBody();
            $body->rewind();
            $userData = json_decode($body->getContents(), JSON_OBJECT_AS_ARRAY);
            return new User($userData['id'], $userData['login']);
        } elseif ($status === 404) {
            throw new DomainException('User with given id not found. ');
        } else {
            throw new RuntimeException('Api responsed with error.', $status);
        }

    }
}