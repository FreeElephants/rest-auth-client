<?php

namespace FreeElephants\RestAuthClient\Exception;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class DomainException extends \DomainException implements AuthClientExceptionInterface
{

}