<?php

namespace FreeElephants\RestAuthClient;

use FreeElephants\RestAuthClient\Exception\RuntimeException;
use FreeElephants\RestAuthClient\HttpAdapter\AdapterInterface;
use FreeElephants\RestAuthClient\Model\UserInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class RestClientTest extends \PHPUnit_Framework_TestCase
{

    public function testGetUser()
    {
        $client = $this->newRestClientInstance();

        $response = $this->createResponse(<<<JSON
{
    "id": 1,
    "login": "user",
    "isSystemUser": false
}
JSON
        );
        $httpClient = $this->createHttpClientMock('get', $response);
        $client->setHttpClient($httpClient);
        $user = $client->getUserByAuthKey('some-auth-key');
        self::assertSame(1, $user->getId());
        self::assertSame('user', $user->getLogin());
    }

    public function testIsValidAuthKey_Positive()
    {
        $response = $this->createResponse('', 200);
        $httpClient = $this->createHttpClientMock('head', $response);
        $client = $this->newRestClientInstance();
        $client->setHttpClient($httpClient);
        self::assertTrue($client->isAuthKeyValid('some-auth-key'));
    }

    public function testIsValidAuthKey_Negative()
    {
        $response = $this->createResponse('', 404);
        $httpClient = $this->createHttpClientMock('head', $response);
        $client = $this->newRestClientInstance();
        $client->setHttpClient($httpClient);
        self::assertFalse($client->isAuthKeyValid('some-auth-key'));
    }

    public function testIsValidAuthKey_Exception()
    {
        $response = $this->createResponse('', 500);
        $httpClient = $this->createHttpClientMock('head', $response);
        $client = $this->newRestClientInstance();
        $client->setHttpClient($httpClient);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionCode(500);
        $client->isAuthKeyValid('some-auth-key');
    }

    public function testGetUsersSuccess()
    {
        $response = $this->createResponse(<<<JSON
{
    "items": [
        {
            "id": 1,
            "login": "login"
        }
    ]
}
JSON
        );
        $httpClient = $this->createHttpClientMock('get', $response);
        $client = $this->newRestClientInstance();
        $client->setHttpClient($httpClient);
        $users = $client->getUsers();
        self::assertCount(1, $users);
        self::assertContainsOnlyInstancesOf(UserInterface::class, $users);
    }

    public function testGetUsersServerErrorHandling()
    {
        $response = $this->createResponse('', 500);
        $httpClient = $this->createHttpClientMock('get', $response);
        $client = $this->newRestClientInstance();
        $client->setHttpClient($httpClient);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionCode(500);
        $client->getUsers();
    }

    public function testGetUserById()
    {
        $response = $this->createResponse(<<<JSON
{
    "id": 1,
    "login": "login"
}
JSON
        );
        $httpClient = $this->createHttpClientMock('get', $response);
        $client = $this->newRestClientInstance();
        $client->setHttpClient($httpClient);
        $user = $client->getUserById(1);
        self::assertSame(1, $user->getId());
        self::assertSame('login', $user->getLogin());
    }

    private function createHttpClientMock(string $method, ResponseInterface $response): AdapterInterface
    {
        $httpClient = $this->createMock(AdapterInterface::class);
        $httpClient->method($method)->willReturn($response);
        return $httpClient;
    }

    private function createResponse(string $body = '', int $status = 200, array $headers = []): ResponseInterface
    {
        $handle = fopen('php://temp', 'w+');
        fwrite($handle, $body);
        $stream = new Stream($handle);
        return new Response($status, $headers, $stream);
    }

    /**
     * @return RestClient
     */
    private function newRestClientInstance()
    {
        $client = new RestClient();
        return $client;
    }
}